\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{eXia_report}[01/02/2013 v1.0]
\LoadClass[12pt, oneside, a4paper]{scrreprt}
\KOMAoptions{
headings=small
}

\RequirePackage[latin1,applemac,utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{textcomp}

\RequirePackage{dejavu}
\normalfont
\addtokomafont{disposition}{\sffamily} 
\renewcommand{\familydefault}{\sfdefault}

\RequirePackage[normalem]{ulem}
\renewcommand{\underline}{\uline} 


\RequirePackage{hyperref}
\hypersetup{
citecolor=black,
filecolor=black,
linkcolor=black,
colorlinks=false,
hypertexnames=false,
pdftitle={\@title - \@subtitle},
pdfsubject={Rapport de projet \@title~-~\@subtitle},
pdfauthor={\@author},
pdfcreator={eXia.cesi - via LaTex}%
}

\RequirePackage[francais,frenchb]{babel}
\RequirePackage[autostyle, babel]{csquotes}
\selectlanguage{french}
\frenchspacing
\FrenchFootnotes

\RequirePackage{graphicx}
\RequirePackage{eso-pic}
\RequirePackage{xcolor}
\RequirePackage{fullpage}
\RequirePackage{framed}
\RequirePackage[top=1cm, bottom=2.5cm, left=1cm, right=1cm,a4paper]{geometry}

\RequirePackage{lscape}
\RequirePackage{pdflscape}


\makeatletter
\def\maketitle{%
\newgeometry{top=0cm, bottom=0cm, left=0cm, right=0cm}
\thispagestyle{empty}
%\clearpage
~
\vfill
\begin{center}
\includegraphics[height=12cm]{../logo.png} 
\end{center}
\vfill
\noindent\rule{\textwidth}{0.1em}
\begin{center}
\textbf{{\Huge \@title}}
\end{center}
\begin{center}
\textbf{{\Huge \@subtitle}}
\end{center}
\noindent\rule{\textwidth}{0.1em}
\vfill
\restoregeometry
}
\makeatother
