Règles de base
==============

- création jeune pousse :
    - 2 voisins arbres
    - 3 voisins arbuste
    - 1 voisin arbre et 2 arbustes

- jeune pousse se transforme en arbuste
    - voisins arbres ou arbuste <= 3
	
- arbuste se transforme en arbre
    - attendre deux tours

---

IHM
===

Base
----
- Taille de la grille (defaut 100x100)
- Nombre de pas
- Vitesse
- **/!\\ Permettre à l'utilisateur de placer les éléments sur la grille**
- Bouton Play
- Boutons Avancer pas à pas
- Afficher le nombre de pas en cours
- Afficher les densités
- Charger/Sauvegarder dans BdD

Feux de forêt
-------------
- **/!\\ Placement du feu sur la grille**
- Afficher les densités en feu

Insectes
--------
- **/!\\ Placement des insectes sur la grille**
- Afficher les densités en insecte

---

Règles avancées
===============

Feux de forêt
-------------
- 2 états s'ajoutent : 
    - en feu
    - en cendre
- Une cellule possède 8 voisins
- Passage à l'état "en feu" :
    - jeune pousse : 25% de chance
    - arbuste: 50% de chance
    - arbres: 75% de chance
- Etat "en feu" dure qu'un tour. Passe à "en cendre"
- Etat "en cendre" dure qu'un tour. Passe à "vide"

Insectes
--------
- 1 état s'ajoute:
    - infecté	
- Une cellule possède 4 voisins
- Passage à l'état "infecté" :
    - jeune pousse : 75% de chance
    - arbuste: 50% de chance
    - arbre: 25% de chance
- Etat "infecté" dure qu'un tour. Passe à "vide"

---

Extras
======

- Avancer/Reculer dans la simulation
- Export des densitées/temps en .csv
- Sauvegarder la totalité de la simulation
