package fr.jsimforest.app.controller;


import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Timer;

import fr.jsimforest.app.model.*;
import fr.jsimforest.app.view.*;

public class Controller implements ActionListener
{
	
	private static volatile Controller instance = null;
	
	private Simulation sim;
	private CellType brush;
	
	private int counter;
	private int maxstep;
	private Timer clock = new Timer(1000, this);;

	private static ArrayList<JsfSimulation> savedSimulations;

    private Controller()
    {
        super();

        this.counter = 0;
        this.brush = CellType.EMPTY;
        Controller.updateSavedSimulationsList();
    }
    
    public final static Controller getInstance() {
        if (Controller.instance == null) {
           synchronized(Controller.class) {
             if (Controller.instance == null) {
            	 Controller.instance = new Controller();
             }
           }
        }
        return Controller.instance;
    }
    
    public void run()
    {
    	/* create simulation */
    	this.sim = new Simulation(new Size(20, 20), Mode.NORMAL);
    	
    	/* start GUI */
        View.getInstance().display();
        
        /* update grid display */
        Window.getInstance().updateGrid(sim.getLastGrid());
    }
    
    public void startSimulation(int maxstep, int speed)
    {
    	//SUPER IMPORTANT !!!!
    	if(!this.clock.isRunning()){
    	this.clock.stop();
    	this.maxstep = maxstep;
    	
    	this.clock = new Timer(speed, this);
		this.clock.start();
    	}
    }
    
    public void stopSimulation()
    {
    	this.clock.stop();
    }
    
    @Override
	public void actionPerformed(ActionEvent e)
	{
		if(this.counter >= this.maxstep)
			this.clock.stop();
		else {
			this.sim.runstep();
			this.sim.setActual(this.counter);
			
			this.setStep(this.counter);
			this.counter++;
		}
		
	}
	
    public void clear()
    {
    	/* create simulation */
    	this.sim = new Simulation(new Size(20, 20), Mode.NORMAL);
    	
    	this.setStep(-1);
    	this.setStep(0);
    }
    
    public void newGrid(Size size)
    {
    	this.sim = new Simulation(size, Mode.NORMAL);
    	
    	this.setStep(-1);
    	this.setStep(0);
    }
    
    public void setBrush(CellType type)
    {
    	this.brush = type;
    }
    
    public CellType getBrush()
    {
    	return this.brush;
    }
    
    public void changeCell(Position pos)
    {
    	Grid g = this.sim.getActualGrid();
    	g.setCell(pos, this.brush);
    	Window.getInstance().updateGrid(g);
    }
    
    public Mode getMode()
    {
    	if (this.sim != null) {
    		return this.sim.getMode();
    	}
    	
    	return Mode.NORMAL;
    }
    
    public void setMode(Mode mode)
    {
    	if (this.sim != null) {
    		this.sim.setMode(mode);
    	}
    }
    
    public void setStep(int step) {
    	Window.getInstance().setStep(step);
    }
    
    public void setCounter(int step)
    {
    	this.counter = step;
    	this.sim.setActual(step);
    	Window.getInstance().updateGrid(sim.getActualGrid());
    }
    

    public Simulation getSimulation()
    {
    	return this.sim;
    }
    
    public void setSimulation(Simulation sim)
    {
    	this.sim = sim;
    	this.setStep(-1);
    	this.setStep(0);
    }

    
    /** TODO: Déplacer ce block **/
    public static List<JsfSimulation> updateSavedSimulationsList(){
    	return Controller.savedSimulations = JsfSimulationCollection.findAll();
    }

    /** TODO: Déplacer ce block **/
    public static ArrayList<JsfSimulation> getSavedSimulationsList(){
    	return Controller.savedSimulations;
    }
    
    


}

