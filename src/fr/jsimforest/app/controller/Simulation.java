package fr.jsimforest.app.controller;

import java.sql.SQLException;
import java.util.*;

import fr.jsimforest.app.model.*;

public class Simulation {
	
	private List<Grid> generations;
	
	private Mode mode;
	
	private Size size;
	
	private int actual;
	
	private JsfSimulation dbRecord = new JsfSimulation();
	
	public Simulation(JsfSimulation record){
		super();
		
		this.dbRecord = record;
		
		this.size = new Size(Integer.parseInt(record.getSimulationWidth()),Integer.parseInt(record.getSimulationHeight()));
		/* TODO: Get last step with right mode */
		this.mode = Mode.NORMAL;
		this.actual = 0;
		
		ArrayList<JsfGrid> grids = JsfGridCollection.findAllGridsFromSimulationId(record.getSimulationId());
		this.generations = new ArrayList<Grid>();		

		for(JsfGrid item : grids ){
			this.generations.add(new Grid(item, this.dbRecord));
			System.out.println(this.getAllGenerations().size());
		}

		
		
		
	}
	
	public Simulation(Size size, Mode mode)
	{
		super();
		this.size = size;
		this.mode = mode;
		this.actual = 0;
		this.generations = new ArrayList<Grid>();
		
		this.generations.add(new Grid(this.size));
	}
	
	public void runstep()
	{
		/* create first grid */
		if (this.generations.isEmpty()) {
			this.generations.add(new Grid(this.size));
		}
		
		/* create second grid */
		Grid g = new Grid(this.size);
		Grid last = this.getActualGrid();
		for (int ypos = 0; ypos < this.size.h; ypos++){
		    for (int xpos = 0; xpos < this.size.w; xpos++)
		    {
		    	Cell c = last.processCell(new Position(xpos, ypos));
		    	g.replaceCell(new Position(xpos, ypos), c);
		    }
		}
		
		g.updateDensity();
		
		if (this.generations.size() - 1 <= this.actual ) {
			this.generations.add(g);
		} else {
			this.generations.set(this.actual + 1, g);
		}
		
		this.actual++;
	}
	
	public Grid getLastGrid()
	{
		int i = this.generations.size() - 1;
		return this.generations.get(i);
	}
	
	public void setActual(int step)
	{
		this.actual = step;
	}
	
	public Grid getActualGrid()
	{
		try {
			return this.generations.get(this.actual);
		}
		catch (IndexOutOfBoundsException e) {
			return this.generations.get(0);
		}
	}
	
	public Grid getGrid(int index)
	{
		try {
			return this.generations.get(index);
		}
		catch (IndexOutOfBoundsException e) {
			return this.generations.get(0);
		}
	}
	
	public void addGrid(Grid grid)
	{
		this.generations.add(grid);
	}
	
	
	public List<Grid> getAllGenerations()
	{
		return this.generations;
	}
	
	public void setMode(Mode mode)
	{
		this.mode = mode;
	}
	
	public Mode getMode()
	{
		return this.mode;
	}
	
	public JsfSimulation getRecord(){
		return this.dbRecord;
	}
	
	private JsfSimulation setRecord(JsfSimulation dbRecord){
		this.dbRecord = dbRecord;
		return this.dbRecord;
	}
	
	public void open(JsfSimulation dbSim) {
		Controller.getInstance().setSimulation(new Simulation(dbSim));

		
		
		
	}
	
	public void save(){
		try {
			this.dbRecord.save();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

