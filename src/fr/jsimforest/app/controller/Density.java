package fr.jsimforest.app.controller;

import java.text.DecimalFormat;

public class Density {
	private double dEmpty;
	private double dSapling;
	private double dBush;
	private double dTree;
	private double dInfested;
	private double dFire;
	
	public Density(double dEmpty, double dSapling, double dBush,
			       double dTree, double dInfested, double dFire)
	{
		super();
		this.dEmpty = dEmpty;
		this.dSapling = dSapling;
		this.dBush = dBush;
		this.dTree = dTree;
		this.dInfested = dInfested;
		this.dFire = dFire;
	}

	public void set(double dEmpty, double dSapling, double dBush,
		            double dTree, double dInfested, double dFire)
	{
		this.dEmpty = dEmpty;
		this.dSapling = dSapling;
		this.dBush = dBush;
		this.dTree = dTree;
		this.dInfested = dInfested;
		this.dFire = dFire;
	}
	
	public String toCsvString() {
		return dEmpty + ";" + dSapling + ";" + dBush + ";" + dTree + ";"
				+ dInfested + ";" + dFire;
	}
	
	@Override
	public String toString() {
		DecimalFormat sEmpty = new DecimalFormat("##.##");
		DecimalFormat sSapling = new DecimalFormat("##.##");
		DecimalFormat sBush = new DecimalFormat("##.##");
		DecimalFormat sTree = new DecimalFormat("##.##");
		DecimalFormat sInfested = new DecimalFormat("##.##");
		DecimalFormat sFire = new DecimalFormat("##.##");
		
		int m = 2;
		sEmpty.setMinimumFractionDigits(m);
		sSapling.setMinimumFractionDigits(m);
		sBush.setMinimumFractionDigits(m);
		sTree.setMinimumFractionDigits(m);
		sInfested.setMinimumFractionDigits(m);
		sFire.setMinimumFractionDigits(m);
		
		return "Densités : Vide : " + sEmpty.format(dEmpty) + " | Pousses : " + sSapling.format(dSapling)
				+ " | Arbuste : " + sBush.format(dBush) + " | Arbres : " + sTree.format(dTree) + " | Insectes : "
				+ sInfested.format(dInfested) + " | Feu : " + sFire.format(dFire);
	}
}
