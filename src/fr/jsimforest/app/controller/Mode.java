package fr.jsimforest.app.controller;

public enum Mode {
	NORMAL (0),
    FIRE (1),
    SWARM (2);
    
    private final int i;   

    Mode(int i) {
        this.i = i;
    }
    
    public static Mode fromInteger(int x) {
        switch(x) {
        case 0:
            return NORMAL;
        case 1:
            return FIRE;
        case 2:
            return SWARM;
        }
        return NORMAL;
    }
    
    public int i() { 
        return i; 
    }
}
