package fr.jsimforest.app.view;


import java.awt.*;
import java.awt.event.*;

import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.filechooser.*;

import fr.jsimforest.app.controller.Controller;
import fr.jsimforest.app.controller.Mode;
import fr.jsimforest.app.model.CellType;
import fr.jsimforest.app.model.Grid;
import fr.jsimforest.app.model.Model;
import fr.jsimforest.app.model.Size;

public final class Window extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7695979703727952471L;

	private static volatile Window instance = null;

	private Display display;

	private JSpinner spinner_step;
	private JSlider slider_step;
	
	private JSpinner spinner_SimulationSteps;
	private JSpinner spinner_SimulationSpeed;
	
	private JLabel label_density;

	private JTextField textField_size_x;

	private JTextField textField_size_y;

	private Window()
	{
		super();
		//option
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setName("MainWindow");
		setTitle("J-SIM Forest Edition");
		
		
		try {
			String CurrentLAF = UIManager.getLookAndFeel().getID();
			if(CurrentLAF.compareToIgnoreCase("Aqua") != 0 && CurrentLAF.compareToIgnoreCase("GTK") != 0){
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if ("Nimbus".equals(info.getName())) {
						UIManager.setLookAndFeel(info.getClassName());
						break;
					}
				}
			}
		} catch (Exception e) {
			
		}

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

		/* left panel */
		JPanel panel_left = new JPanel();
		getContentPane().add(panel_left);
		GridBagLayout gbl_panel_left = new GridBagLayout();
		gbl_panel_left.columnWidths = new int[]{543, 0};
		gbl_panel_left.rowHeights = new int[]{10, 10, 0};
		gbl_panel_left.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_left.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		panel_left.setLayout(gbl_panel_left);

		JPanel panel_display = new JPanel();
		GridBagConstraints gbc_panel_display = new GridBagConstraints();
		gbc_panel_display.fill = GridBagConstraints.BOTH;
		gbc_panel_display.insets = new Insets(0, 0, 5, 0);
		gbc_panel_display.gridx = 0;
		gbc_panel_display.gridy = 0;
		panel_left.add(panel_display, gbc_panel_display);
		panel_display.setLayout(new BoxLayout(panel_display, BoxLayout.Y_AXIS));

		final JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(16);

		/**
		 * Evenement pour eviter une position non régulière du viewport
		 */
		scrollPane.getHorizontalScrollBar().addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e)
			{
				/**
				 * TODO: Un moyen de simplifier le calcul ?
				 */
				scrollPane.getHorizontalScrollBar().setValue(((((int)scrollPane.getHorizontalScrollBar().getValue()+1)/16))*16);
			}
		});

		/**
		 * Evenement pour eviter une position non régulière du viewport
		 */
		scrollPane.getVerticalScrollBar().addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e)
			{
				/**
				 * TODO: Un moyen de simplifier le calcul ?
				 */
				scrollPane.getVerticalScrollBar().setValue(((((int)scrollPane.getVerticalScrollBar().getValue()+1)/16))*16);
			}
		});

		panel_display.add(scrollPane);


		this.display = new Display(Window.class.getResource("/resources/assets/poke2.png"), 16);
		final Display display = this.display;
		scrollPane.setViewportView(display);

		JPanel panel_controls = new JPanel();
		GridBagConstraints gbc_panel_controls = new GridBagConstraints();
		gbc_panel_controls.anchor = GridBagConstraints.SOUTH;
		gbc_panel_controls.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel_controls.gridx = 0;
		gbc_panel_controls.gridy = 1;
		panel_left.add(panel_controls, gbc_panel_controls);
		panel_controls.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		JPanel panel_grid = new JPanel();
		panel_controls.add(panel_grid);
		GridBagLayout gbl_panel_grid = new GridBagLayout();
		gbl_panel_grid.columnWidths = new int[]{107, 71, 350, 0};
		gbl_panel_grid.rowHeights = new int[]{10, 10, 0};
		gbl_panel_grid.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_grid.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_grid.setLayout(gbl_panel_grid);

		JLabel label_step = new JLabel("Pas actuel:");
		GridBagConstraints gbc_label_step = new GridBagConstraints();
		gbc_label_step.anchor = GridBagConstraints.WEST;
		gbc_label_step.insets = new Insets(0, 0, 5, 5);
		gbc_label_step.gridx = 0;
		gbc_label_step.gridy = 0;
		panel_grid.add(label_step, gbc_label_step);

		this.spinner_step = new JSpinner();
		final JSpinner spinner_step = this.spinner_step;
		GridBagConstraints gbc_spinner_step = new GridBagConstraints();
		gbc_spinner_step.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinner_step.insets = new Insets(0, 0, 5, 5);
		gbc_spinner_step.gridx = 1;
		gbc_spinner_step.gridy = 0;
		panel_grid.add(spinner_step, gbc_spinner_step);

		this.slider_step = new JSlider();
		final JSlider slider_step = this.slider_step;
		GridBagConstraints gbc_slider = new GridBagConstraints();
		gbc_slider.fill = GridBagConstraints.HORIZONTAL;
		gbc_slider.insets = new Insets(0, 0, 5, 5);
		gbc_slider.gridx = 2;
		gbc_slider.gridy = 0;

		slider_step.setValue(0);
		slider_step.setMinimum(0);
		slider_step.setMaximum(0);

		panel_grid.add(slider_step, gbc_slider);

		/**
		 * TODO: Comment
		 */
		slider_step.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int s = (int) slider_step.getValue();
				spinner_step.setValue(s);
				Controller.getInstance().setCounter(s);
			}
		});

		
		spinner_step.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent e)
			{
				int s = (int) spinner_step.getValue();
				slider_step.setValue(s);
			}
		});
		


		this.label_density = new JLabel("Densités:");
		GridBagConstraints gbc_label_density = new GridBagConstraints();
		gbc_label_density.gridwidth = 3;
		gbc_label_density.anchor = GridBagConstraints.WEST;
		gbc_label_density.insets = new Insets(0, 0, 0, 5);
		gbc_label_density.gridx = 0;
		gbc_label_density.gridy = 1;
		panel_grid.add(this.label_density, gbc_label_density);

		/* controls panel */

		/* right panel */
		JPanel panel_right = new JPanel();
		getContentPane().add(panel_right);
		panel_right.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		/* right grid */
		JPanel panel_right_grid = new JPanel();
		panel_right.add(panel_right_grid);
		GridBagLayout gbl_panel_right_grid = new GridBagLayout();
		gbl_panel_right_grid.columnWidths = new int[]{0, 0};
		gbl_panel_right_grid.rowHeights = new int[]{10, 10, 10, 10, 10, 10, 0};
		gbl_panel_right_grid.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_panel_right_grid.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_right_grid.setLayout(gbl_panel_right_grid);

		/* panel new */
		JPanel panel_new = new JPanel();
		GridBagConstraints gbc_panel_new = new GridBagConstraints();
		gbc_panel_new.fill = GridBagConstraints.BOTH;
		gbc_panel_new.insets = new Insets(0, 0, 5, 0);
		gbc_panel_new.gridx = 0;
		gbc_panel_new.gridy = 0;
		panel_right_grid.add(panel_new, gbc_panel_new);
		panel_new.setLayout(new GridLayout(0, 1, 0, 0));

		JButton button_new = new JButton("Nouvelle simulation");
		button_new.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/picture_empty.png")));
		panel_new.add(button_new);

		JButton button_open = new JButton("Ouvrir une simulation");
		button_open.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/folder_picture.png")));
		panel_new.add(button_open);

		/* panel size */
		JPanel panel_size = new JPanel();
		panel_size.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Taille de la grille", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_size = new GridBagConstraints();
		gbc_panel_size.fill = GridBagConstraints.BOTH;
		gbc_panel_size.insets = new Insets(0, 0, 5, 0);
		gbc_panel_size.gridx = 0;
		gbc_panel_size.gridy = 1;
		panel_right_grid.add(panel_size, gbc_panel_size);
		panel_size.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));

		final JTextField textField_size_x = new JTextField();
		this.setTextFieldSizeX(textField_size_x);
		textField_size_x.setText("20");
		textField_size_x.setColumns(4);
		panel_size.add(textField_size_x);

		JLabel label = new JLabel("x");
		panel_size.add(label);


		final JTextField textField_size_y = new JTextField();
		this.setTextFieldSizeY(textField_size_y);
		textField_size_y.setText("20");
		textField_size_y.setColumns(4);
		panel_size.add(textField_size_y);

		JButton button_setGridSize = new JButton("Définir");
		button_setGridSize.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/arrow_inout.png")));
		panel_size.add(button_setGridSize);

		/* panel tileset */
		JPanel panel_tileset = new JPanel();
		panel_tileset.setBorder(new TitledBorder(null, "Placement", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_tileset = new GridBagConstraints();
		gbc_panel_tileset.fill = GridBagConstraints.BOTH;
		gbc_panel_tileset.insets = new Insets(0, 0, 5, 0);
		gbc_panel_tileset.gridx = 0;
		gbc_panel_tileset.gridy = 2;
		panel_right_grid.add(panel_tileset, gbc_panel_tileset);



		JButton button_dropEmpty = new JButton("");
		button_dropEmpty.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_empty.png")));
		panel_tileset.add(button_dropEmpty);

		JButton button_dropSapling = new JButton("");
		button_dropSapling.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_sapling.png")));
		panel_tileset.add(button_dropSapling);

		final JButton button_dropBush = new JButton("");
		button_dropBush.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_bush.png")));
		panel_tileset.add(button_dropBush);

		JButton button_dropTree = new JButton("");
		button_dropTree.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_tree.png")));
		panel_tileset.add(button_dropTree);

		final JButton button_dropInfested = new JButton("");
		button_dropInfested.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_infested.png")));
		panel_tileset.add(button_dropInfested);

		final JButton button_dropFire = new JButton("");
		button_dropFire.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/brush_fire.png")));
		panel_tileset.add(button_dropFire);




		/* panel mode */
		JPanel panel_mode = new JPanel();
		panel_mode.setBorder(new TitledBorder(null, "Mode de simulation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_mode = new GridBagConstraints();
		gbc_panel_mode.fill = GridBagConstraints.BOTH;
		gbc_panel_mode.insets = new Insets(0, 0, 5, 0);
		gbc_panel_mode.gridx = 0;
		gbc_panel_mode.gridy = 3;
		panel_right_grid.add(panel_mode, gbc_panel_mode);
		panel_mode.setLayout(new GridLayout(0, 1, 0, 0));

		JRadioButton radioButton_mode_normal = new JRadioButton("Normal");
		panel_mode.add(radioButton_mode_normal);

		JRadioButton radioButton_mode_fire = new JRadioButton("Feu de forêt");
		panel_mode.add(radioButton_mode_fire);

		JRadioButton radioButton_mode_infested = new JRadioButton("Invasion");
		panel_mode.add(radioButton_mode_infested);

		ButtonGroup modeGroup = new ButtonGroup();

		modeGroup.add(radioButton_mode_normal);
		modeGroup.add(radioButton_mode_infested);
		modeGroup.add(radioButton_mode_fire);

		button_dropInfested.setEnabled(false);
		button_dropFire.setEnabled(false);
		radioButton_mode_normal.setSelected(true);

		radioButton_mode_normal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_dropInfested.setEnabled(false);
				button_dropFire.setEnabled(false);
				Controller.getInstance().setBrush(CellType.EMPTY);
				Controller.getInstance().setMode(Mode.NORMAL);
			}
		});

		radioButton_mode_fire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_dropInfested.setEnabled(false);
				button_dropFire.setEnabled(true);
				Controller.getInstance().setBrush(CellType.EMPTY);
				Controller.getInstance().setMode(Mode.FIRE);
			}
		});

		radioButton_mode_infested.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_dropInfested.setEnabled(true);
				button_dropFire.setEnabled(false);
				Controller.getInstance().setBrush(CellType.EMPTY);
				Controller.getInstance().setMode(Mode.SWARM);
			}
		});


		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(null, "Options de simulation", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.insets = new Insets(0, 0, 5, 0);
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 4;
		panel_right_grid.add(panel_6, gbc_panel_6);
		panel_6.setLayout(new GridLayout(0, 2, 0, 12));

		JLabel label_1 = new JLabel("Nombre de pas:");
		panel_6.add(label_1);

		this.spinner_SimulationSteps = new JSpinner(new SpinnerNumberModel(100, 0, 90000, 1));
		final JSpinner spinner_SimulationSteps = this.spinner_SimulationSteps;
		panel_6.add(spinner_SimulationSteps);

		JLabel label_2 = new JLabel("Vitesse en ms:");
		panel_6.add(label_2);

		this.spinner_SimulationSpeed = new JSpinner(new SpinnerNumberModel(250, 0, 90000, 1));
		final JSpinner spinner_SimulationSpeed = this.spinner_SimulationSpeed;
		panel_6.add(spinner_SimulationSpeed);

		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 0;
		gbc_panel_7.gridy = 5;
		panel_right_grid.add(panel_7, gbc_panel_7);
		panel_7.setLayout(new GridLayout(0, 1, 0, 0));

		JButton button_start = new JButton("Lancer");
		button_start.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/control_play_blue.png")));
		panel_7.add(button_start);

		JButton button_stop = new JButton("Arrêter");
		button_stop.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/control_stop_blue.png")));
		panel_7.add(button_stop);

		JButton button_reset = new JButton("Réinitialiser");
		button_reset.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/arrow_rotate_clockwise.png")));
		panel_7.add(button_reset);

		JButton button_save = new JButton("Enregistrer");
		button_save.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/picture_save.png")));
		panel_7.add(button_save);

		JButton button_export = new JButton("Exporter");
		button_export.setIcon(new ImageIcon(Window.class.getResource("/resources/assets/table_save.png")));
		panel_7.add(button_export);


		/**
		 * Capture le click sur le bouton Nouveau
		 */
		button_new.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				int x = Integer.parseInt(textField_size_x.getText());
				int y = Integer.parseInt(textField_size_y.getText());

				Controller.getInstance().newGrid(new Size(x, y));
			}
		});

		/**
		 * Capture le click sur le bouton Ouvrir
		 */
		button_open.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				OpenWindow.getInstance().setVisible(true);
			}
		});

		/**
		 * Capture le click sur le bouton Définir
		 */
		button_setGridSize.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int x = Integer.parseInt(textField_size_x.getText());
				int y = Integer.parseInt(textField_size_y.getText());

				Controller.getInstance().newGrid(new Size(x, y));
			}
		});

		/**
		 * Capture le click sur le bouton Lancer
		 */
		button_start.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controller.getInstance().startSimulation((int)spinner_SimulationSteps.getValue(), (int)spinner_SimulationSpeed.getValue());
			}
		});

		/**
		 * Capture le click sur le bouton Arrêter
		 */
		button_stop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Controller.getInstance().stopSimulation();
				}
		});

		/**
		 * Capture le click sur le bouton Réinitialiser
		 */
		button_reset.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				Controller.getInstance().setStep(0);
			}
		});

		/**
		 * Capture le click sur le bouton Enregistrer
		 */
		button_save.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				SaveWindow.getInstance().setVisible(true);
			}
		});

		/**
		 * Capture le click sur le bouton Exporter
		 */
		button_export.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				final JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"*.csv (Comma separated values)", "csv");
				fc.setFileFilter(filter);
				
				if (fc.showSaveDialog(Window.getInstance()) == JFileChooser.APPROVE_OPTION) {
					Model.getInstance().export(fc.getSelectedFile());
		        } else {

		        }


			}
		});

		/**
		 * Capture le click sur le bouton de placement Vide
		 */
		button_dropEmpty.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				Controller.getInstance().setBrush(CellType.EMPTY);
				display.setCursor(Window.class.getResource("/resources/assets/cursor_empty.png"));
			}
		});

		/**
		 * Capture le click sur le bouton de placement pousses
		 */
		button_dropSapling.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				Controller.getInstance().setBrush(CellType.SAPLING);
				display.setCursor(Window.class.getResource("/resources/assets/cursor_sapling.png"));
			}
		});


		/**
		 * Capture le click sur le bouton de placement arbuste
		 */
		button_dropBush.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				Controller.getInstance().setBrush(CellType.BUSH);
				display.setCursor(Window.class.getResource("/resources/assets/cursor_bush.png"));
			}
		});


		/**
		 * Capture le click sur le bouton de placement arbre
		 */
		button_dropTree.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				Controller.getInstance().setBrush(CellType.TREE);
				display.setCursor(Window.class.getResource("/resources/assets/cursor_tree.png"));
			}
		});

		/**
		 * Capture le click sur le bouton de placement insectes
		 */
		button_dropInfested.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{

				if(button_dropInfested.isEnabled()){
					Controller.getInstance().setBrush(CellType.INFESTED);
					display.setCursor(Window.class.getResource("/resources/assets/cursor_infested.png"));
					Window.playSound("/resources/assets/brush_infested.wav");
				}
			}
		});

		/**
		 * Capture le click sur le bouton de placement feu
		 */
		button_dropFire.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent e)
			{
				if(button_dropFire.isEnabled()){
					Controller.getInstance().setBrush(CellType.FIRE);
					display.setCursor(Window.class.getResource("/resources/assets/cursor_fire.png"));
					Window.playSound("/resources/assets/brush_fire.wav");

				}

			}	
		});
	}

	public void updateGrid(Grid grid)
	{
		this.display.drawGrid(grid);
		this.label_density.setText(grid.getDensity().toString());
	}

	public final static Window getInstance() {
		if (Window.instance == null) {
			synchronized(Window.class) {
				if (Window.instance == null) {
					Window.instance = new Window();
				}
			}
		}
		return Window.instance;
	}

	public final static Window getInstance(int w, int h) {
		Window.getInstance();
		Window.instance.setSize(w, h);
		return Window.instance;
	}

	public void setSize(int w, int h){
		super.setSize(w, h);
		super.setMinimumSize(new Dimension(w, h));
	}

	public static synchronized void playSound(final String url) {

		new Thread(new Runnable() { // the wrapper thread is unnecessary, unless it blocks on the Clip finishing, see comments
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(Window.class.getResource(url));
					clip.open(inputStream);
					clip.start(); 
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}).start();
	}

	public void setStep(int step){
		this.slider_step.setMaximum(step);
		this.slider_step.setValue(step);
		
		if (step >= 0){
			this.slider_step.setMinimum(0);
		}
	}
	
	public void setSpeed(int speed)
	{
		this.spinner_SimulationSpeed.setValue(speed);
	}

	public void setMaxstep(int step)
	{
		this.spinner_SimulationSteps.setValue(step);
	}

	public JTextField getTextFieldSizeX() {
		return textField_size_x;
	}

	public void setTextFieldSizeX(JTextField textField_size_x) {
		this.textField_size_x = textField_size_x;
	}

	public JTextField getTextFieldSizeY() {
		return textField_size_y;
	}

	public void setTextFieldSizeY(JTextField textField_size_y) {
		this.textField_size_y = textField_size_y;
	}
}


