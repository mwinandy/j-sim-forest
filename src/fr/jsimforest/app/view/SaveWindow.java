package fr.jsimforest.app.view;

import javax.swing.*;

import fr.jsimforest.app.controller.Controller;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class SaveWindow extends JDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = -266499525992794807L;
	private static volatile  SaveWindow instance;
	private JTextField textField_simLabel;
	
	private SaveWindow() {
		super();
		
		final SaveWindow window = this;
		
		setSize(new Dimension(300, 90));
		setMaximumSize(new Dimension(300, 90));
		setMinimumSize(new Dimension(300, 90));
		setTitle("Sauvegarde");
		setAlwaysOnTop(true);
		
		JPanel panel_1 = new JPanel();
		panel_1.setAlignmentY(Component.TOP_ALIGNMENT);
		getContentPane().add(panel_1, BorderLayout.NORTH);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 135, 0, 154, 0};
		gbl_panel_1.rowHeights = new int[]{16, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 0;
		panel_1.add(horizontalStrut, gbc_horizontalStrut);
		
		JLabel lblNomDeLa = new JLabel("Nom de la simulation:");
		GridBagConstraints gbc_lblNomDeLa = new GridBagConstraints();
		gbc_lblNomDeLa.anchor = GridBagConstraints.EAST;
		gbc_lblNomDeLa.insets = new Insets(0, 0, 5, 5);
		gbc_lblNomDeLa.gridx = 1;
		gbc_lblNomDeLa.gridy = 0;
		panel_1.add(lblNomDeLa, gbc_lblNomDeLa);
		
		textField_simLabel = new JTextField();
		GridBagConstraints gbc_textField_simLabel = new GridBagConstraints();
		gbc_textField_simLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_simLabel.insets = new Insets(0, 0, 5, 5);
		gbc_textField_simLabel.gridx = 2;
		gbc_textField_simLabel.gridy = 0;
		panel_1.add(textField_simLabel, gbc_textField_simLabel);
		textField_simLabel.setColumns(10);
		

		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut_1 = new GridBagConstraints();
		gbc_horizontalStrut_1.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut_1.gridx = 3;
		gbc_horizontalStrut_1.gridy = 0;
		panel_1.add(horizontalStrut_1, gbc_horizontalStrut_1);
		
		JButton buttonCancel = new JButton("Annuler");
		buttonCancel.setIcon(new ImageIcon(SaveWindow.class.getResource("/resources/assets/cancel.png")));
		GridBagConstraints gbc_buttonCancel = new GridBagConstraints();
		gbc_buttonCancel.anchor = GridBagConstraints.EAST;
		gbc_buttonCancel.insets = new Insets(0, 0, 0, 5);
		gbc_buttonCancel.gridx = 1;
		gbc_buttonCancel.gridy = 1;
		panel_1.add(buttonCancel, gbc_buttonCancel);
		
		JButton buttonSave = new JButton("Sauvegarder");
		buttonSave.setIcon(new ImageIcon(SaveWindow.class.getResource("/resources/assets/picture_save.png")));
		GridBagConstraints gbc_buttonSave = new GridBagConstraints();
		gbc_buttonSave.insets = new Insets(0, 0, 0, 5);
		gbc_buttonSave.anchor = GridBagConstraints.EAST;
		gbc_buttonSave.gridx = 2;
		gbc_buttonSave.gridy = 1;
		panel_1.add(buttonSave, gbc_buttonSave);
		setResizable(false);
	
		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.setVisible(false);				
				textField_simLabel.setText("");
				}
		});
		
		buttonSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				/**
				 * TODO: Revoir le process d'enresgistrement
				 */
				
				Controller.getInstance().getSimulation().getRecord().setSimulationLabel(textField_simLabel.getText());
				Controller.getInstance().getSimulation().save();
				Controller.updateSavedSimulationsList();
				window.setVisible(false);
				
				/**
				 * TODO: Mieux gerer l'effacement
				 */
				textField_simLabel.setText("");
				}
		});
		
	}
	
	public final static SaveWindow getInstance() {
        if (SaveWindow.instance == null) {
           synchronized(OpenWindow.class) {
             if (SaveWindow.instance == null) {
            	 SaveWindow.instance = new SaveWindow();
             }
           }
        }
        return SaveWindow.instance;
    }

}
