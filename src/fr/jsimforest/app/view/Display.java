package fr.jsimforest.app.view;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import fr.jsimforest.app.controller.Controller;
import fr.jsimforest.app.model.Cell;
import fr.jsimforest.app.model.Grid;
import fr.jsimforest.app.model.Position;

public class Display extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7211046479491298303L;
	private Image tileset;
	private BufferedImage img;
	
	private int tile_size;
	
	private int size_x;
	private int size_y;
	
	private Cursor cursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
	
	public Display(URL tileset_file, int tile_size)
	{
		super();
		
		this.tile_size = tile_size;
		
		try {
		    this.tileset = ImageIO.read(tileset_file);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Image de tileset introuvable.");
		}
		
		this.mouseEventListener();
		this.addMouseMotionListener(this.mouseMotionGridViewportListener());
	}
	

	private MouseMotionAdapter mouseMotionGridViewportListener(){
		
		return new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				Position pos = new Position((((int)e.getX())-4)/16, (((int)e.getY())-4)/16);
					Controller.getInstance().changeCell(pos);
			}
		};
	}

	private void mouseEventListener(){
		final Display display = this;

		this.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e) {
				Position pos = new Position((((int)e.getX())-4)/16, (((int)e.getY())-4)/16);
				Controller.getInstance().changeCell(pos);
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				display.setCursor(display.getCursor());
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseExited(MouseEvent e) {
				display.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		});
	}
	
	public Cursor getCursor(){
		return this.cursor;
	}
	
	public void setCursor(URL url){
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.getImage(url);  
		Point hotSpot = new Point(0,0);
		this.cursor = toolkit.createCustomCursor(image, hotSpot, "Pencil");
		super.setCursor(cursor);
	}
	
	public void drawGrid(Grid grid)
	{
		int number = 0;
		int maxX = grid.getSize().w;
		int maxY = grid.getSize().h;
		int size = this.tile_size;
		int sp = 0;
		
		/* set component size */
		this.size_x = maxX * size;
		this.size_y = maxY * size;
		
		/* create surface */
		this.img = new BufferedImage(this.size_x, this.size_x, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = img.createGraphics();
		
		/* draw grid */
		for (int ypos = 0; ypos < maxY; ypos++){
		    for (int xpos = 0; xpos < maxX; xpos++)
		    {
		    	Cell c = grid.getCell(new Position(xpos, ypos));
		    	number = c.getType().i();
		        g.drawImage(this.tileset,
		            xpos * size + sp, ypos * size + sp, xpos * size + size, ypos * size + size,
		            number*size, 0, number*size+size, size,
		            null);
		    }
		}
		
		Dimension d = new Dimension(this.size_x, this.size_y);
		this.setSize(d);
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		
		this.repaint();
	}
	
	
	
	@Override
	public Dimension getMaximumSize() {
		return new Dimension(this.size_x, this.size_y);
	}

	@Override
	public Dimension getMinimumSize() {
		return new Dimension(this.size_x, this.size_y);
	}

	@Override
	public Dimension getSize() {
		return new Dimension(this.size_x, this.size_y);
	}

	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(this.size_x, this.size_y);
	}
	
	@Override
	public void paint(Graphics g)
	{
		g.drawImage(this.img, 0, 0, null);
	}
}
