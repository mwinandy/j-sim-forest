package fr.jsimforest.app.view;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;


import fr.jsimforest.app.controller.*;
import fr.jsimforest.app.model.*;


public final class OpenWindow extends JDialog {


	/**
	 * 
	 */
	private static final long serialVersionUID = -3807715346953674938L;
	private static volatile  OpenWindow instance;
	private JList<JsfSimulation> simulationList;

	private OpenWindow(){
		super();

		final OpenWindow window = this;

		setIconImage(Toolkit.getDefaultToolkit().getImage(OpenWindow.class.getResource("/resources/assets/folder_picture.png")));
		setResizable(false);

		setAlwaysOnTop(true);

		setSize(new Dimension(420, 380));
		setMaximumSize(new Dimension(420, 380));
		setMinimumSize(new Dimension(420, 380));
		setName("OpenSimWindow");
		setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setTitle("Ouvrir une simulation");
		getContentPane().setLayout(new GridLayout(1, 0, 0, 0));

		JPanel panel = new JPanel();
		getContentPane().add(panel);

		panel.setBorder(BorderFactory.createTitledBorder("Simulation à charger"));
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 0.0};
		panel.setLayout(gbl_panel);

		DefaultListModel<JsfSimulation> model = new DefaultListModel<JsfSimulation>();

		for(JsfSimulation item : Controller.getSavedSimulationsList()){
			model.addElement(item);
		}

		final JList<JsfSimulation> simulationList = new JList<JsfSimulation>(model);
		this.simulationList = simulationList;
		
		simulationList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		GridBagConstraints gbc_list = new GridBagConstraints();
		gbc_list.gridwidth = 2;
		gbc_list.insets = new Insets(0, 0, 5, 0);
		gbc_list.fill = GridBagConstraints.BOTH;
		gbc_list.gridx = 0;
		gbc_list.gridy = 0;
		panel.add(simulationList, gbc_list);

		JButton buttonCancel = new JButton("Annuler");
		buttonCancel.setIcon(new ImageIcon(OpenWindow.class.getResource("/resources/assets/cancel.png")));
		GridBagConstraints gbc_buttonCancel = new GridBagConstraints();
		gbc_buttonCancel.anchor = GridBagConstraints.EAST;
		gbc_buttonCancel.insets = new Insets(0, 0, 0, 5);
		gbc_buttonCancel.gridx = 0;
		gbc_buttonCancel.gridy = 1;
		panel.add(buttonCancel, gbc_buttonCancel);

		buttonCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.setVisible(false);
			}
		});

		JButton buttonOpen = new JButton("Ouvrir la simulation");
		buttonOpen.setIcon(new ImageIcon(OpenWindow.class.getResource("/resources/assets/picture_go.png")));
		GridBagConstraints gbc_buttonOpen = new GridBagConstraints();
		gbc_buttonOpen.anchor = GridBagConstraints.EAST;
		gbc_buttonOpen.gridx = 1;
		gbc_buttonOpen.gridy = 1;
		panel.add(buttonOpen, gbc_buttonOpen);

		buttonOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				window.setVisible(false);
				Controller.getInstance().getSimulation().open(simulationList.getSelectedValue());
				Window.getInstance().getTextFieldSizeX().setText(Controller.getInstance().getSimulation().getRecord().getSimulationWidth());
				Window.getInstance().getTextFieldSizeY().setText(Controller.getInstance().getSimulation().getRecord().getSimulationHeight());
				/** TODO: corriger le setter **/
				Window.getInstance().setSpeed(Integer.parseInt(Controller.getInstance().getSimulation().getRecord().getSimulationSpeed()));
				Window.getInstance().setMaxstep(Integer.parseInt(Controller.getInstance().getSimulation().getRecord().getSimulationStep()));

			}
		});
		
		this.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				updateSimulationsList();
			}
		});

	}

	public final static OpenWindow getInstance() {
		if (OpenWindow.instance == null) {
			synchronized(OpenWindow.class) {
				if (OpenWindow.instance == null) {
					OpenWindow.instance = new OpenWindow();
				}
			}
		}
		return OpenWindow.instance;
	}

	public void updateSimulationsList(){
		DefaultListModel<JsfSimulation> model = new DefaultListModel<JsfSimulation>();

		for(JsfSimulation item : Controller.getSavedSimulationsList()){
			model.addElement(item);
		}

		this.simulationList.setModel(model);
	}


}
