package fr.jsimforest.app.view;

public final class View
{
	
	private static volatile View instance = null;
	

    
    private View()
    {
        super();
        Window.getInstance(800,600);
    }
    
    public final static View getInstance() {
        //Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet d'éviter un appel coûteux à synchronized, 
        //une fois que l'instanciation est faite.
        if (View.instance == null) {
           // Le mot-clé synchronized sur ce bloc empêche toute instanciation multiple même par différents "threads".
           // Il est TRES important.
           synchronized(View.class) {
             if (View.instance == null) {
            	 View.instance = new View();
             }
           }
        }
        return View.instance;
    }
    
    public void display()
    {
    	Window.getInstance().setVisible(true);
    }
}

