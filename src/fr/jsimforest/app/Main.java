package fr.jsimforest.app;

import java.io.*;

import org.apache.commons.io.IOUtils;

import fr.jsimforest.app.controller.*;

public class Main
{

	/**
	 * Bootstrap pour l'initialisation de l'instance.
	 * 
	 * @author Jérôme Martin <jerome.martin2@viacesi.fr>
	 */
	static private void bootstrap(){

		/**
		 * Teste si une bdd existe, le cas échéant, l'application se charge de copié la base par défaut.
		 * 
		 * @author Maxence Winandy <maxence.winandy@gmail.com>
		 */
		File db = new File("jsimDB.db");
		if(! db.exists()){
			try {
				InputStream input = Main.class.getResourceAsStream("/resources/database/database.db");
				OutputStream output = new FileOutputStream(db);
				IOUtils.copy(input, output);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		Controller.getInstance().run();
	}


	/**
	 * Fonction main.
	 * 
	 * @param args
	 * @author Maxence Winandy <maxence.winandy@gmail.com>
	 */
	static public void main(String[] args)
	{
		Main.bootstrap();
	}
}


