package fr.jsimforest.app.model;

import java.sql.*;
import java.util.*;

public class JsfSimulationCollection {
	
	public static ArrayList<JsfSimulation> findAll(){
		ArrayList<JsfSimulation> collection = new ArrayList<JsfSimulation>();
		try {
			ResultSet rs = Database.getInstance().executeQuery("SELECT * FROM jsf_simulation ORDER BY simulation_id DESC");
			while(rs.next()){
				collection.add(new JsfSimulation(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return collection;
	}
	

}
