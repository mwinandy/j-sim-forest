package fr.jsimforest.app.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import fr.jsimforest.app.controller.Controller;
import fr.jsimforest.app.controller.Mode;
import fr.jsimforest.app.controller.Simulation;
import fr.jsimforest.app.view.Window;

public class Model
{
	
	private static volatile Model instance = null;
	 
    
    private Model()
    {
        super();
    }
    
    public final static Model getInstance() {
        if (Model.instance == null) {
           synchronized(Model.class) {
             if (Model.instance == null) {
            	 Model.instance = new Model();
             }
           }
        }
        return Model.instance;
    }
    
    
    public void export(File file)
    {
    	try {
    		  /* Create file */
    		  BufferedWriter f = new BufferedWriter(new FileWriter(file));
    		  
    		  /* write densities */
    		  Simulation sim = Controller.getInstance().getSimulation();
    		  f.write("vide;jeune pousse;arbuste;arbre;insectes;feu");
    		  
    		  for (Grid g : sim.getAllGenerations())
    		  {
    			  f.newLine();
    			  f.write(g.getDensity().toCsvString());
    		  }
    		  
    		  /* Close file */
    		  f.close();
    		  
    	} catch (Exception e){
    		e.printStackTrace();
    	}
    }
    
}

