package fr.jsimforest.app.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JsfSimulation {

	private ResultSet record = null;
	private Boolean is_new = true;
	private String simulation_id = null;
	private String simulation_label = null;
	private String simulation_width = null;
	private String simulation_height = null;
	private String simulation_speed = null;
	private String simulation_step = null;
	
	private ArrayList<JsfGrid> grids = null;
	

	static public JsfSimulation findOneById(String simulation_id) throws SQLException{
		ResultSet rs = Database.getInstance().executeQuery("SELECT * FROM jsf_simulation WHERE simulation_id = \""+simulation_id+"\" LIMIT 1");
		rs.next();
		return new JsfSimulation(rs);
	}



	public JsfSimulation(){
		this.simulation_label = "new";
		this.simulation_width = "20";
		this.simulation_height = "20";
		this.simulation_speed = "250";
		this.simulation_step = "100";
	}

	public JsfSimulation(ResultSet rs){
		try {
			this.simulation_id = rs.getString("simulation_id");
			this.simulation_label = rs.getString("simulation_label");
			this.simulation_width = rs.getString("simulation_width");
			this.simulation_height = rs.getString("simulation_height");
			this.simulation_speed = rs.getString("simulation_speed");
			this.simulation_step = rs.getString("simulation_step");
			this.setIsNew(false);
			this.setRecord(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void setIsNew(boolean b) {
		this.is_new = b;
	}

	private boolean isNew() {
		return this.is_new;
	}

	public void delete(){
		/* TODO */
	}

	public JsfSimulation save() throws SQLException{

		//if(this.isNew() || true){

			PreparedStatement stmnt = Database.getInstance().getConnection().prepareStatement("INSERT INTO jsf_simulation (" +
					"simulation_label," +
					"simulation_width," +
					"simulation_height," +
					"simulation_speed," +
					"simulation_step" +
					") " +
					"VALUES( " +
					"?," +
					"?," +
					"?," +
					"?," +
					"? " +
					");");

			stmnt.setString(1, this.simulation_label);
			stmnt.setString(2, this.simulation_width);
			stmnt.setString(3, this.simulation_height);
			stmnt.setString(4, this.simulation_speed);
			stmnt.setString(5, this.simulation_step);


			stmnt.executeUpdate();

			ResultSet generatedKeys = Database.getInstance().executeQuery("SELECT seq FROM sqlite_sequence WHERE name=\"jsf_simulation\";");
			if (generatedKeys.next()) {
				this.setSimulationId(generatedKeys.getString(1));
			} else {
				throw new SQLException("Creating failed, no generated key obtained.");
			}
/*
		}else{
			
			
			//TODO: Gérer enregistrer sous

			PreparedStatement stmnt = Database.getInstance().getConnection().prepareStatement("UPDATE jsf_simulation SET " +
					"simulation_label = ?," +
					"simulation_width = ?," +
					"simulation_height = ?," +
					"simulation_step = ?," +
					"simulation_speed = ? " +
					"WHERE simulation_id = ?;");

			stmnt.setString(1, this.simulation_label);
			stmnt.setString(2, this.simulation_width);
			stmnt.setString(3, this.simulation_height);
			stmnt.setString(4, this.simulation_step);
			stmnt.setString(5, this.simulation_speed);
			stmnt.setString(6, this.simulation_id);

			stmnt.executeUpdate();
		}
*/
		return this;
	}

	public String toString(){
		return (String)this.simulation_label;
	}

	public String getSimulationId(){
		return this.simulation_id;
	}

	public JsfSimulation setSimulationId(String simulation_id){
		this.simulation_id = simulation_id;
		return this;
	}

	public String getSimulationLabel(){
		return this.simulation_label;
	}

	public JsfSimulation setSimulationLabel(String simulation_label){
		this.simulation_label = simulation_label;
		return this;
	}

	public String getSimulationWidth(){
		return this.simulation_width;
	}
	
	public JsfSimulation setSimulationWidth(String simulation_width){
		this.simulation_width = simulation_width;
		return this;
	}

	public String getSimulationHeight(){
		return this.simulation_height;
	}
	
	public JsfSimulation setSimulationHeight(String simulation_height){
		this.simulation_height = simulation_height;
		return this;
	}

	public String getSimulationSpeed(){
		return this.simulation_speed;
	}
	
	public JsfSimulation setSimulationSpeed(String simulation_speed){
		this.simulation_speed = simulation_speed;
		return this;
	}

	public String getSimulationStep(){
		return this.simulation_step;
	}
	
	public JsfSimulation getSimulationStep(String simulation_step){
		this.simulation_step = simulation_step;
		return this;
	}
	
	public ResultSet getRecord() {
		return this.record;
	}

	private JsfSimulation setRecord(ResultSet record) {
		this.record = record;
		return this;
	}
	


}
