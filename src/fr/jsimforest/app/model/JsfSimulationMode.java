package fr.jsimforest.app.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JsfSimulationMode {

	private ResultSet record = null;
	private Boolean is_new = true;
	private String simulation_mode_id = null;
	private String simulation_mode_mnemonic = null;
	private String simulation_mode_label = null;

	static public JsfSimulationMode findOneById(String simulation_mode_id) throws SQLException{
		ResultSet rs = Database.getInstance().executeQuery("SELECT * FROM jsf_simulation_mode WHERE simulation_mode_id = \""+simulation_mode_id+"\" LIMIT 1");
		return new JsfSimulationMode(rs);
	}

	static public JsfSimulationMode findOneByMnemonic(String simulation_mode_id) throws SQLException{
		ResultSet rs = Database.getInstance().executeQuery("SELECT * FROM jsf_simulation_mode WHERE simulation_mode_mnemonic = \""+simulation_mode_id+"\" LIMIT 1");
		return new JsfSimulationMode(rs);
	}


	public JsfSimulationMode(){
		this.simulation_mode_id = null;
		this.simulation_mode_mnemonic = null;
		this.simulation_mode_label = null;
	}

	public JsfSimulationMode(ResultSet rs){
		try {
			rs.next();
			this.simulation_mode_id = rs.getString("simulation_mode_id");
			this.simulation_mode_mnemonic = rs.getString("simulation_mode_mnemonic");
			this.simulation_mode_label = rs.getString("simulation_mode_label");
			this.setIsNew(false);
			this.setRecord(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	

	private void setIsNew(boolean b) {
		this.is_new = b;
	}

	private boolean isNew() {
		return this.is_new;
	}

	public void delete(){
		/* TODO */
	}

	public JsfSimulationMode save() throws SQLException{

		if(this.isNew()){

			PreparedStatement stmnt = Database.getInstance().getConnection().prepareStatement("INSERT INTO jsf_simulation_mode (" +
					"simulation_mode_label," +
					"simulation_mode_mnemonic," +
					") " +
					"VALUES( " +
					"?," +
					"?," +
					");");

			stmnt.setString(1, this.simulation_mode_label);
			stmnt.setString(2, this.simulation_mode_mnemonic);

			stmnt.executeUpdate();

			ResultSet generatedKeys = Database.getInstance().executeQuery("SELECT seq FROM sqlite_sequence WHERE name=\"jsf_simulation_mode\";");
			if (generatedKeys.next()) {
				this.setSimulationModeId(generatedKeys.getString(1));
			} else {
				throw new SQLException("Creating failed, no generated key obtained.");
			}

		}else{

			PreparedStatement stmnt = Database.getInstance().getConnection().prepareStatement("UPDATE jsf_simulation_mode SET " +
					"simulation_mode_label = ?," +
					"simulation_mode_mnemonic = ? " +
					"WHERE simulation_mode_id = ?;");
			
			stmnt.setString(1, this.simulation_mode_label);
			stmnt.setString(2, this.simulation_mode_mnemonic);
			stmnt.setString(3, this.simulation_mode_id);

			stmnt.executeUpdate();
		}

		return this;
	}


	public String getSimulationModeId() {
		return simulation_mode_id;
	}



	public JsfSimulationMode setSimulationModeId(String simulation_mode_id) {
		this.simulation_mode_id = simulation_mode_id;
		return this;
	}



	public String getSimulationModeMnemonic() {
		return simulation_mode_mnemonic;
	}



	public JsfSimulationMode setSimulationModeMnemonic(String simulation_mode_mnemonic) {
		this.simulation_mode_mnemonic = simulation_mode_mnemonic;
		return this;
	}



	public String getSimulationModeLabel() {
		return simulation_mode_label;
	}



	public JsfSimulationMode setSimulationModeLabel(String simulation_mode_label) {
		this.simulation_mode_label = simulation_mode_label;
		return this;
	}

	public ResultSet getRecord() {
		return this.record;
	}

	private JsfSimulationMode setRecord(ResultSet record) {
		this.record = record;
		return this;
	}

}
