package fr.jsimforest.app.model;

import java.sql.*;

public final class Database
{

	private static volatile Database instance = null;

	private static Connection connection = null;

	
	
    private Database() throws ClassNotFoundException
    {
    	super();
    	Class.forName("org.sqlite.JDBC");
    	
    	try
        {
          Database.connection = DriverManager.getConnection("jdbc:sqlite:jsimDB.db");
        }
        catch(SQLException e)
        {
          System.err.println(e.getMessage());
        }
    }
    
    
    public Connection getConnection()
    {
    	return Database.connection;
    }

    public final static Database getInstance() {
        if (Database.instance == null) {
           synchronized(Database.class) {
             if (Database.instance == null) {
            	 try {
					Database.instance = new Database();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
             }
           }
        }
        return Database.instance;
    }
    
    public Statement getStatement(){
    	Statement statement = null;
		try {
			statement = Database.connection.createStatement();
			statement.setQueryTimeout(30);
			return statement;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }
    
    public ResultSet executeQuery(String query) throws SQLException{
    	return this.getStatement().executeQuery(query);
    }
    
    public int executeUpdate(String query) throws SQLException{
    	return this.getStatement().executeUpdate(query);
    }
    
}

