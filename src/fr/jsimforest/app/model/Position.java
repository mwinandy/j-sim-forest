package fr.jsimforest.app.model;

public class Position
{
    public int x;
    public int y;

    public Position(int x, int y)
    {
        super();
        this.set(x, y);
    }
    
    public void set(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public Position extend(int dx, int dy)
    {
    	return new Position(this.x+dx, this.y + dy);
    }
}


