package fr.jsimforest.app.model;

import fr.jsimforest.app.controller.Controller;
import fr.jsimforest.app.controller.Density;
import fr.jsimforest.app.controller.Mode;

public class Grid
{
    private Size size;
    private Cell[][] cells;
    
    private Density density;
	private JsfGrid dbRecord;

    
    public Grid(JsfGrid record, JsfSimulation sim)
    {
        super();
        this.size = new Size(Integer.parseInt(sim.getSimulationWidth()), Integer.parseInt(sim.getSimulationHeight()));
        this.dbRecord = record;
        
        /* create cell table */
        this.cells = new Cell[size.h][size.w];
        for (int y = 0; y < size.h; y++){
		    for (int x = 0; x < size.w; x++)
		    {
		    	this.cells[y][x] = new Cell(CellType.EMPTY);
		    }
		}
        
        this.density = new Density(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    }
    
    
    public Grid(Size size)
    {
        super();
        this.size = size;
        
        /* create cell table */
        this.cells = new Cell[size.h][size.w];
        for (int y = 0; y < size.h; y++){
		    for (int x = 0; x < size.w; x++)
		    {
		    	this.cells[y][x] = new Cell(CellType.EMPTY);
		    }
		}
        
        this.density = new Density(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
    }
    
    public Cell processCell(Position pos)
    {
    	Cell c = this.getCell(pos);
    	CellType ct = c.getType();
    	Mode m = Controller.getInstance().getMode();
    	
    	Cell nc = new Cell(CellType.EMPTY);
    	nc.setLife(c.getLife());
    	
    	/* check mode */
    	switch (m){
    	
    	/* normal mode  */
    	case NORMAL:
        	switch (ct){
        	case EMPTY:
        		nc.setType(this.checkCellEmpty(pos));
        		return nc;
        	case SAPLING:
        		nc.setType(this.checkCellSapling(pos));
        		return nc;
        	case BUSH:
        		nc.setType(this.checkCellBush(nc));
        		return nc;
        	case TREE:
        		return c;
        	case INFESTED:
        		return nc;
        	case FIRE:
        		return nc;
        	case ASH:
        		return nc;
			default:
				break;
        	};
        	break;
        	
        /* fire mode */
    	case FIRE:
    		switch (ct){
    		case SAPLING:
        		nc.setType(this.checkCellSaplingFire(pos));
        		return nc;
        	case BUSH:
        		nc.setType(this.checkCellBushFire(pos));
        		return nc;
        	case TREE:
        		nc.setType(this.checkCellTreeFire(pos));
        		return nc;
        	case ASH:
        		return nc;
        	case FIRE:
        		nc.setType(CellType.ASH);
        		return nc;
        	case INFESTED:
        		return nc;
			default:
				break;
        	};
        	break;
        	
        /* swarm mode */
    	case SWARM:
    		switch (ct){
    		case SAPLING:
        		nc.setType(this.checkCellSaplingSwarm(pos));
        		return nc;
        	case BUSH:
        		nc.setType(this.checkCellBushSwarm(pos));
        		return nc;
        	case TREE:
        		nc.setType(this.checkCellTreeSwarm(pos));
        		return nc;
        	case INFESTED:
        		return nc;
        	case ASH:
        		return nc;
        	case FIRE:
        		return nc;
			default:
				break;
        	};
        	break;
    	}
    	
    	return c;
    }
    
    public CellType checkCellEmpty(Position pos)
    {
    	CellType[] l = this.getCellMooreNeighborhood(pos);
    	
    	int nTree = 0;
    	int nBush = 0;
    	
    	/* count trees and bushes */
    	for (int i = 0; i < 8; i++){
    		if (l[i] == CellType.TREE)
    			nTree++;
    		else if (l[i] == CellType.BUSH)
    			nBush++;
    	}
    	
    	/* rule */
    	if (nTree >= 2 || nBush >= 3 || (nTree == 1 && nBush == 2))
    		return CellType.SAPLING;
    	
    	return CellType.EMPTY;
    }
    
    public CellType checkCellSapling(Position pos)
    {
    	CellType[] l = this.getCellMooreNeighborhood(pos);
    	
    	int nTree = 0;
    	int nBush = 0;
    	
    	/* count trees and bushes */
    	for (int i = 0; i < 8; i++){
    		if (l[i] == CellType.TREE)
    			nTree++;
    		else if (l[i] == CellType.BUSH)
    			nBush++;
    	}
    	
    	/* rule */
    	if (nTree + nBush <= 3)
    		return CellType.BUSH;
    	
    	return CellType.SAPLING;
    }
    
    public CellType checkCellBush(Cell c)
    {
    	if (c.getLife() < 2){
    		c.grow();
    	} else {
    		return CellType.TREE;
    	}
    	
    	return CellType.BUSH;
    }
    
    public boolean checkCellFire(Position pos)
    {
    	CellType[] l = this.getCellMooreNeighborhood(pos);

    	/* find fire */
    	boolean fire = false;
    	for (int i = 0; i < 8; i++){
    		if (l[i] == CellType.FIRE){
    			fire = true;
    			break;
    		}
    	}
    	
    	return fire;
    }
    
    public boolean checkCellSwarm(Position pos)
    {
    	CellType[] l = this.getCellNeumannNeighborhood(pos);

    	/* find fire */
    	boolean swarm = false;
    	for (int i = 0; i < 4; i++){
    		if (l[i] == CellType.INFESTED){
    			swarm = true;
    			break;
    		}
    	}
    	
    	return swarm;
    }
    
    public CellType checkCellSaplingFire(Position pos)
    {
    	if (checkCellFire(pos))
    		if ((int) (Math.random() * 100) <= 25) // 25% chance
    		return CellType.FIRE;
    	
    	return CellType.SAPLING;
    }
    
    public CellType checkCellBushFire(Position pos)
    {
    	if (checkCellFire(pos))
    		if ((int) (Math.random() * 100) <= 50) // 50% chance
    		return CellType.FIRE;
    	
    	return CellType.BUSH;
    }
    
    public CellType checkCellTreeFire(Position pos)
    {
    	if (checkCellFire(pos))
    		if ((int) (Math.random() * 100) <= 75) // 75% chance
    		return CellType.FIRE;
    	
    	return CellType.TREE;
    }
    
    public CellType checkCellSaplingSwarm(Position pos)
    {
    	if (checkCellSwarm(pos))
    		if ((int) (Math.random() * 100) <= 75) // 75% chance
    		return CellType.INFESTED;
    	
    	return CellType.SAPLING;
    }
    
    public CellType checkCellBushSwarm(Position pos)
    {
    	if (checkCellSwarm(pos))
    		if ((int) (Math.random() * 100) <= 50) // 50% chance
    		return CellType.INFESTED;
    	
    	return CellType.BUSH;
    }
    
    public CellType checkCellTreeSwarm(Position pos)
    {
    	if (checkCellSwarm(pos))
    		if ((int) (Math.random() * 100) <= 25) // 25% chance
    		return CellType.INFESTED;
    	
    	return CellType.TREE;
    }
    
    public CellType[] getCellMooreNeighborhood(Position pos)
    {
    	CellType[] l = new CellType[8];
    	
    	l[0] = this.getCell(pos.extend(-1, -1)).getType(); // up left
    	l[1] = this.getCell(pos.extend( 0, -1)).getType(); // up mid
    	l[2] = this.getCell(pos.extend( 1, -1)).getType(); // up right
    	l[3] = this.getCell(pos.extend(-1,  0)).getType(); // mid left
    	l[4] = this.getCell(pos.extend( 1,  0)).getType(); // mid right
    	l[5] = this.getCell(pos.extend(-1,  1)).getType(); // bot left
    	l[6] = this.getCell(pos.extend( 0,  1)).getType(); // bot mid
    	l[7] = this.getCell(pos.extend( 1,  1)).getType(); // bot right
    	
    	return l;
    }
    
    public CellType[] getCellNeumannNeighborhood(Position pos)
    {
    	CellType[] l = new CellType[4];
    	
    	l[0] = this.getCell(pos.extend( 0, -1)).getType(); // up
    	l[1] = this.getCell(pos.extend(-1,  0)).getType(); // left
    	l[2] = this.getCell(pos.extend( 1,  0)).getType(); // right
    	l[3] = this.getCell(pos.extend( 0,  1)).getType(); // bottom
    	
    	return l;
    }
    
    public void updateDensity()
    {
    	double total = size.w * size.h;
    	double nEmpty = 0;
    	double nSapling = 0;
    	double nBush = 0;
    	double nTree = 0;
    	double nInfested = 0;
    	double nFire = 0;
    	
    	for (int y = 0; y < size.h; y++){
		    for (int x = 0; x < size.w; x++)
		    {
		    	CellType t = this.cells[y][x].getType();
		    	
		    	switch(t){
		    	case ASH:
		    	case EMPTY:
		    		nEmpty++;
		    		break;
		    	case SAPLING:
		    		nSapling++;
		    		break;
		    	case BUSH:
		    		nBush++;
		    		break;
		    	case TREE:
		    		nTree++;
		    		break;
		    	case INFESTED:
		    		nInfested++;
		    		break;
		    	case FIRE:
		    		nFire++;
		    	default:
		    		break;
		    	}
		    }
		}
    	
    	this.density.set(nEmpty/total, nSapling/total, nBush/total, nTree/total, nInfested/total, nFire/total);
    }
    
    public Size getSize()
    {
    	return this.size;
    }
    
    public void setCell(Position pos, CellType type)
    {
    	try{
    		this.cells[pos.y][pos.x].setType(type);
    		this.updateDensity();
    	} catch (ArrayIndexOutOfBoundsException e) {
    		/* do nothing ! */
    	}
    }
    
    public void replaceCell(Position pos, Cell cell)
    {
    	try{
    		this.cells[pos.y][pos.x] = cell;
    		this.updateDensity();
    	} catch (ArrayIndexOutOfBoundsException e) {
    		/* do nothing ! */
    	}
    }
    
    public Cell getCell(Position pos)
    {
    	try{
    		return this.cells[pos.y][pos.x];
    	} catch (ArrayIndexOutOfBoundsException e) {
    		return new Cell(CellType.EMPTY);
    	}
    }
    
    public Density getDensity()
    {
    	return this.density;
    }
}


