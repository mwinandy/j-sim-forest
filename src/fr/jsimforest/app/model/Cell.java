package fr.jsimforest.app.model;

public class Cell
{
    private CellType type;
    private int life;

    public Cell()
    {
        super();
        this.type = CellType.EMPTY;
    	this.life = 1;
    }
    
    public Cell(CellType type)
    {
    	this.type = type;
    	this.life = 1;
    }
    
    public void setType(CellType type)
    {
        this.type = type;
        
        if (this.type != CellType.BUSH) {
        	this.life = 1; // reset life time
        }
    }
    
    public CellType getType()
    {
        return this.type;
    }
    
    public int getLife()
    {
    	return this.life;
    }
    
    public void setLife(int life)
    {
    	this.life = life;
    }
    
    public void grow()
    {
    	this.life = this.life + 1;
    }
}


