package fr.jsimforest.app.model;

public class Size
{
    public int w;
    public int h;

    public Size(int w, int h)
    {
        super();
        this.set(w, h);
    }
    
    public void set(int w, int h)
    {
        this.w = w;
        this.h = h;
    }
}


