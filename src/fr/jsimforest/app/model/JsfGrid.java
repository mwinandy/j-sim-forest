package fr.jsimforest.app.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JsfGrid {
	
	public String grid_step;
	public String simulation_id;
	public String simulation_mode_id;
	private boolean is_new;
	private ResultSet record;

	public JsfGrid(String simulation_id){
		this.grid_step = "0";
		this.simulation_id = simulation_id;
		this.simulation_mode_id = "1";
	}

	public JsfGrid(ResultSet rs) {
		try {
			this.grid_step = rs.getString("grid_step");
			this.simulation_id = rs.getString("simulation_id");
			this.simulation_mode_id = rs.getString("simulation_mode_id");
			this.is_new = false;
			this.record = rs;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
