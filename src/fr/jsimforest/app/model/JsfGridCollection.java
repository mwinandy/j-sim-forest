package fr.jsimforest.app.model;

import java.sql.*;
import java.util.*;

public class JsfGridCollection {
	
	public static ArrayList<JsfGrid> findAllGridsFromSimulationId(String simulation_id){
		ArrayList<JsfGrid> collection = new ArrayList<JsfGrid>();
		try {
			ResultSet rs = Database.getInstance().executeQuery("SELECT * FROM jsf_grid WHERE simulation_id=\""+simulation_id+"\" ORDER BY grid_step ASC");
			while(rs.next()){
				collection.add(new JsfGrid(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return collection;
	}
	

}
